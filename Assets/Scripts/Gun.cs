﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour {

    public GameObject decal;
    public AudioSource audio;
    public float waitTime = 0.15f;
    private float wait = 0;
    public Camera _camera;

    public void shoot()
    {
        if (wait <= 0)
        {
            wait = waitTime;
            audio.Play();

            RaycastHit hit;

            if (Physics.Raycast(_camera.transform.position, _camera.transform.forward, out hit))
            {
                if (hit.collider.tag == "Enemy")
                {
                    hit.transform.SendMessage("damage");
                }
                else
                {
                    GameObject currentDacal = Instantiate<GameObject>(decal);
                    currentDacal.transform.position = hit.point + hit.normal * .01f;
                    currentDacal.transform.rotation = Quaternion.LookRotation(-hit.normal);
                    currentDacal.transform.SetParent(hit.transform);

                    Rigidbody r = hit.transform.GetComponent<Rigidbody>();
                    if (r != null)
                    {
                        r.AddForceAtPosition(-hit.normal * .25f, hit.transform.InverseTransformPoint(hit.point), ForceMode.Impulse);
                    }
                }
            }
        }
    }

    void Update()
    {
        if (wait > 0)
        {
            wait -= Time.deltaTime;
        }
    }
}
