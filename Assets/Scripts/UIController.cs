﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {
    public Text scoresDispley;

	void Start () {
        GameManager.instance.scoreEvent += ScoreChange;
        Cursor.lockState = CursorLockMode.Locked; 
        Cursor.visible = false;  
	}

	void ScoreChange (int scores) {
        if (scoresDispley != null)
        {
            scoresDispley.text = scores.ToString();
        }
	}

    void OnDestroy()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;  
    }
}
