﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour {

    public NavMeshAgent agent;
    public Animator animator;
    private Transform player;

	void Start () {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        StartCoroutine(FindPath());
        StartCoroutine(playerDetect());
	}

    IEnumerator playerDetect()
    {
        while (true)
        {
            if (player == null)
            {
                break;
            }
            if (Vector3.Distance(transform.position, player.position) < 1f)
            {
                animator.SetBool("attack", true);
                player.SendMessage("damage");
            }
            else
            {
                animator.SetBool("attack", false);
            }
            yield return new WaitForSeconds(.3f);
        }
    }
    IEnumerator FindPath()
    {
        while (true)
        {
            if (player != null)
            {
                agent.SetDestination(player.position);
                yield return new WaitForSeconds(2f);
            }
            else
            {
                break;
            }
        }
    }

    public void damage()
    {
        StopAllCoroutines();
        agent.enabled = false;
        animator.SetTrigger("die");
        gameObject.GetComponent<CapsuleCollider>().enabled = false;
        Destroy(gameObject, 5.0f);
        GameManager.instance.deadUnit(gameObject);
    }
}
